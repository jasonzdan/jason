# Docker

- Pin OS images (e.g. `jammy-20231211.1`). Not necessary until we need to update
  for a security or feature update, at which point developers will need to
  manually update to ensure that the `latest` image is up to date.
- Ensure that git clones are `--depth 1` (e.g. Intel RealSense). Alternatively,
  fetch tarballs from Github where possible to remove the dependency on git.
- Git clones and dependency tarballs should reference a tag or release where
  possible. Where not possible, a comment should explain the floating reference.
- Use `&&` instead of `;` to chain commands so that image builds will fail on
  error.
- Most image `CMDs` are `bash`. They should typically be the primary executable.
  This can be overridden in `docker-compose` when it makes sense to do so.
- Logging is done via the `syslog` driver. For `docker-compose`, at least, it's
  often better to use the default `stdout`/`stderr` logging so that a log stream
  is available in the IDE/terminal. Standard logging may also be desirable on
  production as well, especially with tooling or drivers designed to ship logs
  elsewhere (e.g. ELK). `syslog` should probably only be used when the final
  destination is `syslog`.
- Time zone should always be UTC. Local time works best when every person and
  every device is in the same time zone. Otherwise UTC is preferable.
- Don't set `DEBIAN_FRONTEND` as an `ENV`. Only prefix commands that require it,
  as otherwise it will affect interactive shell sessions as well.
- Use multistage builds to separate build stage with source code and build
  dependencies from final stage with build artifacts. Depending on the
  differences between development and release builds, this could also
  potentially target different environments. Multistage builds can also be used
  to isolate and improve the cachability of third-party libraries.
- `host` networking should be avoided when possible. In scalable environments
  such as k8s, this restricts services to one pod per host. The typical use case
  of communicating over unprivileged ports does not require `host` networking.
  Under `docker-compose`, publish ports using `host:container` syntax for
  one-to-one mapping. If scaling is a concern, use long syntax with the
  `published: "<start>-<end>"` and `mode: ingress` options.

## Development

- Developers should be using rootless Docker.
- Investigate whether there's any value in [containers.dev][containers-dev].
  These images provide standardized development containers, and
  [CLion][clion-containers-dev] is supported. In this case building the code
  would be an operation within the container rather than a full image build.
  _I'm unsure whether this would be beneficial or not for the current workflow._
- Consider using volumes for `~/techbrew/hss/` instead of bind-mounting if
  appropriate.
- A build cache (for e.g. intermediate object files) may be helpful, depending
  on the development workflow and build size.
- Are `/tmp` mounts for core dumps? If not, consider removing them. If so,
  verify that this works as expected as core dump locations often require
  additional configuration.

## Compose

Compose has a lot of interesting new features, most of which are new to me and
thus untested.

- Use a `name` property to namespace projects.
- Use [fragments] and [extensions] to reuse repeated elements.
- Use [profiles] to specify which subset of services to run. For example,
  `arm-camera-component` would not be started on `up` unless its profile is
  requested.
- Use the [develop] specification to resync and rebuild when watched files
  change.
- Use [configs] for run-time configuration.

[clion-containers-dev]:
  https://blog.jetbrains.com/idea/2023/06/intellij-idea-2023-2-eap-6/#SupportforDevContainers
[configs]: https://docs.docker.com/compose/compose-file/08-configs/
[containers-dev]: http://containers.dev
[develop]: https://docs.docker.com/compose/compose-file/develop/
[extensions]: https://docs.docker.com/compose/compose-file/11-extension/
[fragments]: https://docs.docker.com/compose/compose-file/10-fragments/
[profiles]: https://docs.docker.com/compose/profiles/
